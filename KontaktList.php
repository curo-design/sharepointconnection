<?php	
	namespace VGrem\phpSPO;
	//******************************************************** SHAREPOINT ********************************************************//
	//erste Lib für das Auslesen der Werte von Sharepoint
	include 'SharePointAPI.php';
	//zweite Lib für das Hinzufügen von Kontakte in Sharepoint
	include 'phpSPO-master/src/SPOClient.php';
	include 'phpSPO-master/src/SPList.php';
	include 'phpSPO-master/src/HttpUtilities.php';
	use Thybag\SharePointAPI;


	//Angabe des Pfads +  Username & Passwort zur Anmeldung von Sharepoint
	$sp = new SharePointAPI('auer@curo-design.de', 'JoJa47!$', 'https://kuemmerer.sharepoint.com/teams/web/srs/_vti_bin/Lists.asmx?wsdl', 'SPONLINE');
	//Auslesen der liste 'Kontakte' & umformatieren in XML


	$spArray = $sp->query('Kontakte')->get();
	//print_r($spArray);
	
	$spContact = array();
	for($i=0, $c = count($spArray); $i < $c; $i++) {
		$uniqueid='';
		$sipgateId='';
		$firstname='';
		$surname ='';
		$telefonwork = '';
		$company = '';
		$email ='';
		$lastModified=''; 

		if ($spArray[$i]['uniqueid']) {
			$uniqueid = $spArray[$i]['uniqueid'];
		}
		if ($spArray[$i]['firstname']) {
			$firstname = $spArray[$i]['firstname'];
		}
		if ($spArray[$i]['title']) {
			$surname = $spArray[$i]['title'];
		}
		if ($spArray[$i]['workphone']) {
			$telefonwork = $spArray[$i]['workphone'];
		}
		if ($spArray[$i]['email']) {
			$email = $spArray[$i]['email'];
		}
		if ($spArray[$i]['q8sm']) {
			$sipgateId = $spArray[$i]['q8sm'];
		}
		if ($spArray[$i]['firma']) {
			$company = $spArray[$i]['firma'];
		}
		if ($spArray[$i]['modified']) {
			$lastModified = $spArray[$i]['modified'];
		}
		$spContact[] = array('sharePointId'=>$uniqueid,'sipgateId'=>$sipgateId,'firstname'=>$firstname,'surname'=>$surname,'telefonwork'=>$telefonwork,'email'=>$email,'company'=>$company,'modified'=>$lastModified);
	} 


 

	//******************************************************** SIPGATE ********************************************************//
	//Angabe des Pfads +  Username & Passwort zur Anmeldung von Sharepoint
	$url = "https://info%40curo-design.de:%24FGsze5n@api.sipgate.net/my/contacts/?version=2.18.1&complexity=full&limit=500";
	$sipgate = simplexml_load_file($url);
	//print_r($sipgate);

	//Array mit allen Kontakten aus Sipgate. Das gleiche wie oben bei Sharepoint
	$sgContact = array();
	foreach ($sipgate->items->item as $key => $items) {

		unset($uniqueId);
		$sharePointId = '';
		$givenname = '';
		unset($famname);
		$workphone = '';
		$firm = '';
		$mail = '';
		unset($modified);

		foreach ($items->entries->entry as $key => $entry) {
			switch ($entry->entryName) {
				case 'TEL':
					$workphone = trim($entry->value->__toString());
					break;
				case 'N':
					$famname = trim($entry->family->__toString());
					$givenname= trim($entry->given->__toString());
					break;
				case 'EMAIL':
					$mail = trim($entry->value->__toString());
					break;
				case 'NOTE':
					$sharePointId = trim($entry->value->__toString());
					break; 
			}
		}

		foreach ($items->contactMeta as $key => $meta) {
			if($meta->validFrom) {
				//$modified = new \DateTime($meta->validFrom->__toString());
				$modified = $meta->validFrom->__toString();
				//Das T vor der Uhrezeit mit einem Leerzeichen ersetzen, damit die Daten von Sharepoint und Sipgate gleich formatiert sind
				$modified = str_replace('T',' ',$modified);
			}
			if($meta->UUID)  {
				$uniqueId = trim($meta->UUID->__toString());
			}
		}

		$sgContact[] = array('sipgateId'=>$uniqueId,'sharePointId' =>$sharePointId,'firstname'=>$givenname,'surname'=>$famname,'telefonwork'=>$workphone,'email'=>$mail,'company'=>$firm,'modified'=>$modified);
	}
	//print_r($sgContact);

	//******************************************************** Vergleichen & Hinzufügen in Sipgate ********************************************************//
	/*function compareElem($arr1,$arr2) {
		$arrayDiff = array();
		for($i=0, $c = count($arr1); $i < $c; $i++) {
			if (!array_search($arr1[$i]['surname'],array_column($arr2, 'surname')) && !empty(array_column($arr2,'surname'))) {
				if (!array_search($arr1[$i]['firstname'],array_column($arr2, 'firstname'))) {
					if (!array_search($arr1[$i]['telefonwork'],array_column($arr2, 'telefonwork'))) {
						$arrayDiff[] = $arr1[$i];
					}
				}
			}
		}

		//Kontakte in Sipgate importieren
		for($i=0, $c = count($arrayDiff); $i < $c; $i++) {
			//als erstes alle Variabeln auf '' setzten, damit alter wert gelöscht wird
			$Vorname = '';
			$Nachname = '';
			$Telefon = '';
			$Email = '';
			$Company = '';
			//Variablen setzten
			$Vorname = $arrayDiff[$i]['firstname'];
			$Nachname = $arrayDiff[$i]['surname'];
			$Telefon = $arrayDiff[$i]['telefonwork'];
			$Email = $arrayDiff[$i]['email'];
			$Company = $arrayDiff[$i]['company'];
			$sharePointId = $arrayDiff[$i]['sharePointId'];
			//vCard erstellen
			$newVcard = urlencode('{"EntryList":[{"EntryID":"", "Entry":"BEGIN:VCARD\nN:'.$Nachname.';;;\nFN:'.$Nachname.' '.$Vorname.'\nTEL;HOME;VOICE:'.$Telefon.'\nEMAIL:'.$Email.'\nORG:'.$Company.'\nNOTE:'.$sharePointId.'\nEND:VCARD"}]}');
			//vCard in Sipgate importieren
			$url = "https://info%40curo-design.de:%24FGsze5n@api.sipgate.net/my/xmlrpcfacade/samurai.PhonebookEntrySet/$newVcard";
			$sipgate = file_get_contents($url);
		}	
	}
	echo compareElem($spContact,$sgContact); */

//******************************************************** Vergleichen & Hinzufügen in Sharepoint********************************************************//
/*	Funktion zum Vergleichen, Hinzufügen und Updaten von Kontakten aus Sipgate in Sharepoint.
	Diese Funktion müsste man später aufteilen, da der ganze Abschnitt zum Vergleichen auch in Sipgate übernommen werden kann.
	Nur das Hinzufügen von Kontakten funktioniert bei den beiden Anbietern uterschiedliche.

	Das erste Array ist die Quelle und enthält Kontakte, die man hinzufügen will. Das zweite Array ist das Ziel, in dem die 
	Kontakte hinzugefügt werden.
*/

	function compareSp($arr1,$arr2) {
		//verlängerung der Ausführungszeit, da dass vergleichen und hinzufügen ziemlich lange dauert
		set_time_limit(240);

		//Dieser Block dient zur Initialiserung der SpoPhp lib für Sharepoint
		$username = 'auer@curo-design.de';
		$password = 'JoJa47!$';
		$url = "https://kuemmerer.sharepoint.com/teams/web/srs";
		$client = new SPOClient($url);
		$client->signIn($username,$password);
		$listTitle = 'Kontakte';
		$list = $client->getList($listTitle);
						

		/*	Es wird ein ArrayDiff erstellet, indem die fehlenden Kontakte aus der Quelle, für das Ziel, gespeichert werden. 	
			In der For-Schleife wird getestet ob ein Kontakt schon im Ziel-Array vorhanden ist.	*/
		$arrayDiff = array();
		//Jeder Kontakt aus dem Quelle-Array
		for($i=0, $c = count($arr1); $i < $c; $i++) {
			//Wenn der nachname nicht in dem Ziel-Array existiert, dann weiter testen 
			if (!array_search($arr1[$i]['surname'],array_column($arr2, 'surnames'))) {
				//echo '<pre>';
				//print_r($arr1[$i]['surname']);
				//echo '</pre>';
				//Wenn der Vorname zusammen mit dem Nachnamen nicht existert, weiter testen 
				if (!array_search($arr1[$i]['firstname'],array_column($arr2, 'firstname'))) {
					//print_r($arr1[$i]['firstname']);
					//Wenn der Name mit der Telefonnummer nicht existiert, weiter testen
					if (!array_search($arr1[$i]['telefonwork'],array_column($arr2, 'telefonwork'))) {
						//Wenn die ID dann auch noch nicht existiert, dann füge diesen Kontakt in $arrayDiff
						if (!array_search($arr1[$i]['sipgateId'],array_column($arr2, 'sipgateId'))) {
							$arrayDiff[] = $arr1[$i];	
						}
					}
				}
			}
		} 
		

		/* Test ob ein Update eines Kontaktes nötig ist.
			Es wird die sipgateId jedes Kontaktes des Quelle-Array mit jedem Kontakt aus dem Ziel-Array verglichen  */
		for($i=0, $c = count($arr1); $i < $c; $i++) {
			for($x=0, $c = count($arr2); $x < $c; $x++) {

				if ($arr1[$i]['sipgateId'] == $arr2[$x]['sipgateId']) {
					//falls die Id übereinstimmt, wird getestet welcher der beiden Kontakte am neusten ist
					//falls der Kontakt aus dem Quelle-Array neuer ist, wird in $arrayDiff hinzugefügt
					//echo '<pre>';
					//print_r($arr1[$i]['sipgateId'].' '.$arr1[$i]['surname'] .' == '.$arr2[$x]['sharePointId'].' '.$arr2[$x]['surname']);
					//echo '</pre>';
					if (strtotime($arr1[$i]['modified']) > strtotime($arr2[$x]['modified'])) {	
						print_r($arr1[$i]['sipgateId'].' '.$arr1[$i]['modified'] .' > '.$arr2[$x]['sipgateId'].' '.$arr2[$x]['modified']);
						//Kontakt mit den neuen Werten aufnehmen
						$arrayDiff[] = $arr1[$i];
						//löschen den alten Kontaktes bei Update
						$delete = substr($arr2[$x]['sharePointId'], 0, 4);
						print_r($delete);
						$item = $list->deleteItem($delete);
					}
				}
			}
		} 

		print_r($arrayDiff);
		/*	Kontakte in Sharepoint hinzufügen.
			Jeder Kontakt aus arrayDiff wird in Sharepoint einzelnd, nacheinander hinzugefügt	*/
		for($i=0, $c = count($arrayDiff); $i < $c; $i++) {
			//als erstes alle Variabeln auf '' setzten, damit alter wert gelöscht wird
			unset($itemProperties);
			//Variabeln setzten
			$sipgateId =$arrayDiff[$i]['sipgateId'];
			$Vorname = $arrayDiff[$i]['firstname'];
			$Nachname = $arrayDiff[$i]['surname'];
			$Telefon = $arrayDiff[$i]['telefonwork'];
			$Email = $arrayDiff[$i]['email'];
			$Company = $arrayDiff[$i]['company'];
			//Funktion von SPOphp um einen Kontakt hinzuzufügen
			$itemProperties = array('FirstName'=>$Vorname,'Title' =>$Nachname, 'FullName'=>$Nachname.' '.$Vorname, 'WorkPhone'=>$Telefon,'Email'=>$Email,'q8sm'=>$sipgateId);
			$item = $list->addItem($itemProperties);
		} 
	} 

	echo compareSp($sgContact,$spContact); 

	//Update Funktion von SharePoint Kontakten
	 /*function updateSpKontakt($arr1, $arr2) {
	 	$username = 'auer@curo-design.de';
		$password = 'JoJa47!$';
		$url = "https://kuemmerer.sharepoint.com/teams/web/srs";
		$client = new SPOClient($url);
		$client->signIn($username,$password);
		$listTitle = 'Kontakte';
		$list = $client->getList($listTitle);
		/* Test ob ein Update eines Kontaktes nötig ist.
		Es wird die sipgateId jedes Kontaktes des Quelle-Array mit jedem Kontakt aus dem Ziel-Array verglichen  
		for($i=0, $c = count($arr1); $i < $c; $i++) {
			for($x=0, $c = count($arr2); $x < $c; $x++) {
				if ($arr1[$i]['sipgateId'] == $arr2[$x]['sipgateId']) {
					//falls die Id übereinstimmt, wird getestet welcher der beiden Kontakte am neusten ist
					//falls der Kontakt aus dem Quelle-Array neuer ist, wird in $arrayDiff hinzugefügt
					if (strtotime($arr1[$i]['modified']) > strtotime($arr2[$x]['modified'])) {	
						print_r($arr1[$i]['surname'].' '.$arr1[$i]['modified'] .' > '.$arr2[$x]['surname'].' '.$arr2[$x]['modified']);
						//Kontakt mit den neuen Werten aufnehmen
						$itemID = substr($arr2[$x]['sharePointId'], 0, 4);
						if ($arr1[$i]['surname'] !== $arr2[$x]['surname']) {
							$Nachname = array('Title'=> $arrayDiff[$i]['surname']);
							$list->updateItem($itemID,$Nachname);
						}elseif ($arr1[$i]['firstname'] !== $arr2[$x]['firstname']) {
							$Vorname = array('FirstName',$arrayDiff[$i]['firstname']);
							$list->updateItem($itemID,$Vorname);
						}elseif ($arr1[$i]['telefonwork'] !== $arr2[$x]['telefonwork']) {
							$Telefon = array('WorkPhone',$arrayDiff[$i]['telefonwork']);
							$list->updateItem($itemID,$Telefon);
						}elseif ($arr1[$i]['email'] !== $arr2[$x]['email']) {
							$Email = array('Email',$arrayDiff[$i]['email']);
							$list->updateItem($itemID,$Email);
						}//elseif ($arr1[$i]['company'] !== $arr2[$x]['company']) {
							//$Company = array($arrayDiff[$i]['company']);
						//}
					}
				}
			}
		}
	}

	echo updateSpKontakt($sgContact,$spContact); */

?>